/* old school
function compose() {
  var funcs = Array.prototype.clice.call( aruguments ).reverse();
  return function(){
    return funcs.slice(1).reduce( function(res, un){
      return fn(res);
    }, funcs[0].apply(undefined, arguments) );
    }
}
*/
// const compose = (...fns) => x => fns.reduceRight((y, f) => f(y), x);
function compose(...fns){
  return x => fns.reduceRight((y, f) => f(y), x);
}


 function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

/* OWNER: douglascox, uxi-automation
   * SOURCES: 
   *
   * DESCRIPTION: Upon submission of go/studyrequestform, this script determines 
   * whether the requester is supported by Research Ops, determines which pod
   * should handle this request, and emails the appropriate pod
   *[OPTIONAL] @param {(sheet|object)} - reference sheet for data fetch
   *[OPTIONAL] @param {(number)} - row containing header 0-based index
   *[OPTIONAL] @param {(string|number)} - row where data start 0-based index
   *[OPTIONAL] @return {(object)} - properites of sheet for filter/update/etc
   */

  /* returns properties of sheet
   * @param Object({Sheet})
   * @return { dataRange, body, headerObj, header, getValueByHeader }
   */
  function getDataBySheet({sheet, headerIdx = 0, bodyRowStart = 1}) {
    var dataRange = sheet.getDataRange();
    var values = dataRange.getValues()
    var headers = values.slice(headerIdx, headerIdx + 1)[0];
    let duplicates = headers.reduce( (acc, next) => headers.indexOf(next) === headers.lastIndexOf(next) ? acc : acc.concat(next), []  )
    var body = values.slice(bodyRowStart)
    var headerObj = headers.reduce(makeHeaderObj, {})

    return {
      dataRange,
      body: body.map(getValueByHeader),
      headerObj,
      headers,
      getValueByHeader,
    }
    function makeHeaderObj(acc, next, idx) {
      if (next === "" || next === "-") {
        return acc;
      }
      if (acc.hasOwnProperty(next)) {
        throw (`Duplicate headers found ${next}`)
      }
      acc[next] = idx
      return acc;
    }
    // transform Array(row) => Object()
    function getValueByHeader(row, rowIdx) {
      var rowAsObject = Object.create(headerObj)
      for (var header in headerObj) {
        var idx = headerObj[header]
        rowAsObject[header.trim()] = row[idx]
      }
      Object.defineProperties(rowAsObject, {
        getRangeByColHeader: {
          value: ((header) => headers.includes(header) ? sheet.getRange(rowIdx + 1 + bodyRowStart, headerObj[header] + 1) : (()=> {throw (`header "${header}" not found `)})() ), // return range of column 4
          writable: false,
          enumerable: false,
        },
      })

      return rowAsObject
    }
  }

