function artificalEventGenerator(rowIdx) {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = SpreadsheetApp.getActiveSheet();
  const dataRange = sheet.getDataRange()
  const values = dataRange.getValues();
  const headers = values[0]
  const activeRowIdx = rowIdx || SpreadsheetApp.getActiveRange().getRow();
  const activeRow = values[activeRowIdx - 1]
  const namedValues = getNamedValues()
  // const namedVafslues = getNamedValues(headerBySheet, activeRow);
  const firstRowEl = activeRow[0]
  return { namedValues, range: SpreadsheetApp.getActiveRange() }


  function getNamedValues() {
    if (sheet.getName() === "Form Responses") {
      const colIdxSupportType = headers.indexOf("What type of research support do you need?")
      const researchType = getResearchType(activeRow[colIdxSupportType])
      const colHeaderByType = values[1].reduce((acc, next, idx) => {
        if (next === researchType || next === "rSupport") {
          return acc.concat(values[0][idx])
        } else {
          return acc;
        }
        // ?  : 
        // acc)
      }, ['Is this study request for an Out Of The Office (OOTO) venue? If you select "Yes", it means you have already consulted with the OOTO Team.'])

      return colHeaderByType.reduce((acc, next, idx) => {
        if (next === "") { return acc; }
        var colIdx = values[0].indexOf(next);
        var hasQAValue = activeRow[colIdx]
        acc[next] = hasQAValue !== null ? [activeRow[colIdx]] : [""]
        return acc
      }, {})
      colHeaderByType.forEach((header) => namedValues.hasOwnProperty(header) ? null : namedValues[header] = [""])
      debugger
      return colHeaderByType
    } else {
      return values[0].reduce((acc, next, idx) => {
        if (next === "") { return acc; }
        var hasQAValue = activeRow[colIdx]
        acc[next] = hasQAValue !== null ? [activeRow[colIdx]] : [""]
        // acc[next] = [activeRow[idx]]
        return acc
      }, {})
    }

    return namedValues;

    function getResearchType(type) {
      return {
        "UXI ResearchOps: Self-recruitment support - only need a participant invitation list shared OR access granted to the internal participant database": "ooto",
        "UXI ResearchOps: Full recruitment support - identifying potential study participants, sending out invitations, screening and scheduling": "rOps",
        "3rd party research vendor services through Fast Research team, such as: Moderation, Data analysis, Research report preparation, Facility rental, Transportation, A/V services, Document translation, Simultaneous translation, Transcription, Immersion, Intercept, Survey launch": "fResearch"
      }[type]
    }
  }
}