function onOpen() {
  var ui = SpreadsheetApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('Options')
    .addItem('Multivitiam Test Trigger', 'multivitiamtest')
    // .addItem('Manual "Single" createFormSubmission ', 'manualSingleIntake')
    .addToUi();
}

/* OWNER: douglascox, uxi-automation
 * SOURCES: go/frtrix, go/studyqueue
 *
 * DESCRIPTION: Menu button on trix to execute manual createSubmission event, relative to active range.
 * @param null
 * @return null
 */

function manualSingleIntake() {
  /*
  get active range
  confirm sheet selected
  prompt user on row confirmation
  */
  const selectedSheet = SpreadsheetApp.getActiveSheet()
  const selectedSheetName = selectedSheet.getName()
  const selectRowIdx = SpreadsheetApp.getActiveRange().getRow()
  var isValidSelection = Browser.msgBox(`Manual Submit: row ${selectRowIdx}, on tab name ${selectedSheetName}`, Browser.Buttons.YES_NO) === 'yes' ? true : false

  const sheetNameMaptoFn = {
    'Form Responses': singleIntakeMapping,
    'FR to R-Ops': toRops,
    'R-Ops to FR': toFR
  }
  try{
    Logger.log(`manual run by: ${Session.getActiveUser().getEmail()}`)
  } catch(e){}
  try {
    if (isValidSelection) {
      
      sheetNameMaptoFn.hasOwnProperty(selectedSheetName) ? runManual(sheetNameMaptoFn[selectedSheetName]) : Browser.msgBox("Submission execution not found")
    }

  } catch (e) {
    throw (e)
  }
  var boo = sheetNameMaptoFn.hasOwnProperty(selectedSheetName)
 
  function runManual(fn) {
    var ss = SpreadsheetApp.getActiveSpreadsheet()
    var sheet = SpreadsheetApp.getActiveSheet(); //form response sheet
    var msheet = ss.getSheetByName('Question mapping'); //map sheet
    const e = artificalEventGenerator()
    var rowIdx = e.range.getRow()
    debugger
    fn(ss, sheet, msheet, e)
  }



function artificalEventGenerator() {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = SpreadsheetApp.getActiveSheet();
  const dataRange = sheet.getDataRange()
  const values = dataRange.getValues();
  const headers = values[0]
  const activeRowIdx = SpreadsheetApp.getActiveRange().getRow();
  const activeRow = values[activeRowIdx - 1]
  const namedValues = getNamedValues()
  // const namedVafslues = getNamedValues(headerBySheet, activeRow);
  const firstRowEl = activeRow[0]
  return { namedValues, range: SpreadsheetApp.getActiveRange()}


  function getNamedValues() {
    if (sheet.getName() === "Form Responses") {
      const colIdxSupportType = headers.indexOf("What type of research support do you need?")
      const researchType = getResearchType(activeRow[colIdxSupportType])
      const colHeaderByType = values[1].reduce((acc, next, idx) => {
        if (next === researchType || next === "rSupport") {
          return acc.concat(values[0][idx])
        } else {
          return acc;
        }
        // ?  : 
        // acc)
      }, ['Is this study request for an Out Of The Office (OOTO) venue? If you select "Yes", it means you have already consulted with the OOTO Team.'])
      
      return colHeaderByType.reduce((acc,next,idx) => {
        if(next === "" ) { return acc;}
        var colIdx = values[0].indexOf(next)
        acc[next] = [activeRow[colIdx]]
        return acc
      }, {})
      colHeaderByType.forEach((header) => namedValues.hasOwnProperty(header) ? null : namedValues[header] = [""])
      debugger
      return colHeaderByType
    } else {
      return values[0].reduce((acc,next,idx) => {
        if(next === "" ) { return acc;}
        acc[next] = [activeRow[idx]]
        return acc
      }, {})
    }

    return namedValues;

    function getResearchType(type) {
      return {
        "UXI ResearchOps: Self-recruitment support - only need a participant invitation list shared OR access granted to the internal participant database": "ooto",
        "UXI ResearchOps: Full recruitment support - identifying potential study participants, sending out invitations, screening and scheduling": "rOps",
        "3rd party research vendor services through Fast Research team, such as: Moderation, Data analysis, Research report preparation, Facility rental, Transportation, A/V services, Document translation, Simultaneous translation, Transcription, Immersion, Intercept, Survey launch": "fResearch"
      }[type]
    }
  }
}
}

// Utility Fn
function compose(...fns) {
  return x => fns.reduceRight((y, f) => f(y), x);
}

