

// (set,add) WhomWeSupport Metric
function fetchAndSetWhomWeSupportMetric(e) {
  ["Researcher Location",	"Cost Center Code (recruiters do not edit this column)",	"Cost Center Name (recruiters do not edit this column)",	"Product Area (recruiters do not edit this column)	Are they on the list of supported cost centers?",	"Pod",	"BU Code",	"Manager List",	"unified_rollup_level_1"].forEach( str => delete e.namedValues[str])
  // let rowIdx = 125;
  e = e || artificalEventGenerator(rowIdx);
  /** get email address from form submission */
  let emailKey = Object.keys(e.namedValues).find(str => str.toLowerCase().trim().includes('email'))
  let [emailAddress] = e.namedValues[emailKey]
  let getMetricByEmail = fetchWhomWeSupportMetric()
  /** fetch whom we support metric */
  const whomWeSupportMetrics = getMetricByEmail(emailAddress)
  const fromProps = getRowValues(whomWeSupportMetrics)
  const toProps = getSheetProperties(e)
  /** set whom we support metric on single intake */
  setRowValues(fromProps, toProps)
  let {namedValues} = e
  namedValues = Object.assign(namedValues,{ ...fromProps })
  return e


  function getSheetProperties(e) {
    let range = e.range;
    let sheet = range.getSheet();
    let dataRange = sheet.getDataRange()
    let values = dataRange.getValues()
    let [headerRow, subHeaderRow, ...body] = values
    let rowIdx = range.getRow();
    let row = body[rowIdx - 3]; 
    let researchLocationIdx = headerRow.findIndex( str => "Researcher Location" === str.trim())
    const supportFields = headerRow.slice(researchLocationIdx)
    
    const rowAsObj = supportFields.reduce((acc, next, idx, arr) => {
      idx += researchLocationIdx
        let header = headerRow[idx];
        let value = row[idx];
        let rowAsObject = { [header]: { getRangeOfHeader: () => sheet.getRange(rowIdx, idx + 1) } }
        return Object.assign(acc, rowAsObject)
      return acc
    }, {})
    return rowAsObj
  }



  function fetchWhomWeSupportMetric() {
    const devID = "1s7hcB2ipDz2bTfbl4v61rjU9N9pmPXgm1BsLKEnYf_Q"
    // const id = "1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A";
    const getSSById = (id) => SpreadsheetApp.openById(devID);
    const allLdapsSheet = getSSById(devID).getSheetByName("All ldaps")
    let { body } = getDataBySheet({ sheet: allLdapsSheet })

    return function (emailAsStr) {
      let ldapFromEmail = emailAsStr.indexOf('@') === -1 ? emailAsStr.trim() : emailAsStr.slice(0, emailAsStr.indexOf('@'));
      const propsByLdap = ({ ldap }) => ldap === ldapFromEmail
      let response = Object.assign({}, body.find(propsByLdap)) || null
       
      return response
      // return body.find(propsByLdap) || null
    }
    function whomWeSupportDataByLdap({ ldap }) {
      const isSameStr = ldap === emailAsStr;
      const isBlank = emailAsStr !== ""
      return isSameStr && !isBlank
    }
  }


  function setRowValues(rowAsObj, whomWeSupportMetric) {
    let trixHeaders = Object.keys(whomWeSupportMetric)
    const setRangeByHeader = (header) => {
      try {
        let range = whomWeSupportMetric.hasOwnProperty(header) ? whomWeSupportMetric[header].getRangeOfHeader() : null
        let currValue = range.getValue()
        let [nextValue] = rowAsObj[header] !== null ? rowAsObj[header] : [""]
        currValue === "" && range.setValue(nextValue)
      } catch (e) {
        throw (e)
      }
    }
    trixHeaders.forEach(setRangeByHeader);
    return rowAsObj
  }

  function getRowValues(props) {
    const hasPropMatchGen = (object, str) => object.hasOwnProperty(str)
    const hasPropMatchLower = (object, str) => object.hasOwnProperty(str.toLowerCase())
    const hasPropMatchLowTrim = (object, str) => object.hasOwnProperty(str.toLowerCase().trim())
    const hasPropsByName = (object, propName) => [hasPropMatchGen, hasPropMatchLowTrim, hasPropMatchLower].some(fn => fn(object, propName))
    const asProp = (fromHeaderName, toHeaderName) =>
      (props) =>
        Object.assign({}, { [`${fromHeaderName}`]: (hasPropsByName(props, toHeaderName) ? [props[toHeaderName]] : [null]) })

    return [
      asProp("Researcher Location", "Location"),
      asProp("Cost Center Code (recruiters do not edit this column)", "Cost Center Code"),
      asProp("Cost Center Name (recruiters do not edit this column)", "Cost Center Name"),
      asProp("Product Area (recruiters do not edit this column)", "Business Unit"),
      asProp("Are they on the list of supported cost centers?", "Support level"),
      asProp("Pod", "pod"),
      asProp("BU Code", "BU Code"),
      asProp("Manager List", "Manager List"),
      asProp("unified_rollup_level_1", "unified_rollup_level_1"),
    ].map(oMaker => oMaker(props))
      .reduce((acc, next) => Object.assign(acc, next), {})

  }
}

function getAgencyType({namedValues}){
  let {"Pod": podArr} = namedValues
  let [podName] = podArr
  let regexAnswerLabTest = /al/gi.test(podName);
  const agency = regexAnswerLabTest ? "answerlab" : "infosys";
  return {agency}
}