function createFormSubmission_backup() {
  //open spreadsheet
  var ss = SpreadsheetApp.getActiveSpreadsheet()
  var sheet = SpreadsheetApp.getActiveSheet(); //form response sheet
  
  var ms = ss.getSheetByName('Question mapping'); //map sheet
  var mapValues = ms.getDataRange().getValues();
  var msLastRow = ms.getLastRow();
  
  var ks = ss.getSheetByName('Question 1 Key'); //question 1 key sheet
  var keyValues = ks.getDataRange().getValues();
  var ksLastRow = ks.getLastRow();
  var map = {};
  for (var v = 1; v<ksLastRow; v++){
    map[keyValues[v][0]] = keyValues[v][1];
  }
  
  var formResponse = e.range.getValues()[0];
  var activeRow = e.range.getRow(); //row_number
  var values = e.namedValues; //headers and form submission answers
  var headers = sheet.getRange(1,1,1,sheet.getLastColumn()).getValues()[0];

//---------------------------------------------------------------------
//question 1 mapping
 var qMap = {};
 var supportTypes = values[map["rSupport"]][0];
    Logger.log('supportTypes: ' + supportTypes);
 
  for(var v=0; v<keyValues.length; v++){
    if(keyValues[v][1] == supportTypes.trim()){
      supportTypes = keyValues[v][0];
        Logger.log('supportTypes: ' + supportTypes);
    }
    var formId = keyValues[v][2];
      Logger.log('formId: ' + formId);
  }
  qMap["rSupport"] = "";
  
    Logger.log('participant type index - rOps: ' + supportTypes.indexOf("rOps"));
  if(supportTypes.indexOf("rOps") >=0 ){
    qMap["rOps"] = "Yes";
    qMap["rSupport"] += "rOps"+"<br>";
  }
  else{
    qMap["rOps"] = "No";
  }
    Logger.log('qMap["rOps"]: ' + qMap["rOps"]);
//---------------------------------------------------------------------   
  /*
  for (var i in headers)
    if (values[headers[i]] != ''){
      Logger.log(headers[i] + ' - ' + values[headers[i]]);
    }
  */
  
    Logger.log(values[map["rSupport"]][0]);
  var array = [];
  
  for(var i = 0; i < headers.length; i++){
    //if(values[headers[1]].toString().indexOf("UXI ResearchOps") >= 0){ //change to look at key
    if(qMap["rOps"] == 'Yes'){
        //Logger.log('Studies request')
      if(values[headers[i]] != ''){
        for(var j = 1; j < msLastRow; j++){
          if(mapValues[j][0] == headers[i]){
            var line = [];
              //Logger.log('E2E question number: ' + mapValues[j][0]); //use this number in the question array[]
              //Logger.log('Study Q question number: ' + mapValues[j][1]); //use this number in the answer array[]
            line.push(mapValues[j][0]);
            line.push(mapValues[j][1]);
              Logger.log(line);
            array.push(line);
          }
        }
      }
    } 
    else { //FR section
        //Logger.log('FR request')
      if(values[headers[i]] != ''){
        for(var j = 1; j < msLastRow; j++){
          if(mapValues[j][0] == headers[i]){
            var line = [];
              Logger.log('E2E question number: ' + mapValues[j][0]);
              Logger.log('Study Q question number: ' + mapValues[j][2]);
            line.push(mapValues[j][0]);
            line.push(mapValues[j][2]);
              Logger.log(line);
            array.push(line);
          }
        }
      }  
    }
    
  }
    //Logger.log('array: ' + array); //?might make new function to use this array which is what we needed out of this
    //var array2 = [];
    //array2.push(array);
    //array2.push(values[headers[1]].toString().indexOf("UXI ResearchOps")); //question 1 index so we know what survey the answers get mapped to
      Logger.log('array: ' + array); //possibly make this the new array we pass with the mapping array, question1 index and other info (maybe timestamp and ldap).  Unless we do all in one. (ask Janice)
      //Logger.log(array2[0][2][1]); //second entry in third question of the mapping array - worked!
      //Logger.log(array2[1]);
      
  var testArray = [];
  /*if(qMap["rOps"] == 'Yes'){
    var form = FormApp.openByUrl('https://docs.google.com/forms/d/1z3v-6YFtnNAGwLFVX3XaqKvPSY6eS2-gER_s8oxJ9xA/edit'); //Study Q form
  }
  else {
    var form = FormApp.openByUrl('https://docs.google.com/forms/d/1RERVXWGZovib4p_dxhFHCwfaoDRazCK_Yvs45TepC24/edit'); //FR form
  }*/
    var form = FormApp.openByUrl(formId);
    var questions = form.getItems();
    var formResponse = form.createResponse();
    for(var n = 0; n < questions.length; n++){
        //Logger.log('questionTitle: ' + questions[n].asTextItem().getTitle());
      for(var k = 0; k < array.length; k++){
          //Logger.log('array[0][k]: ' + array[k][0]);
          //Logger.log('array[1][k]: ' + array[k][1]);
        if(array[k][1] == questions[n].getTitle()){
          var line = [];
          var mappingQ = array[k][0];
            Logger.log('mappingQ: ' + mappingQ);
          var column = headers.indexOf(mappingQ);
            Logger.log('column: ' + column);
          var questionType = questions[n].getType();
            Logger.log('questionType: ' + n + ' ' + questionType);
          var answer = sheet.getRange(activeRow,column+1,1,1).getValue(); //Add something to grab a blank for space holder
            Logger.log('answer: ' + answer);
          /*line.push(mappingQ);
          line.push(answer);
          testArray.push(line);*/
          //formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
          
          if (questions[n].getType() == 'TEXT'){
            formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
          }
          else if (questions[n].getType() == 'DATE'){
            formResponse = formResponse.withItemResponse(questions[n].asDateItem().createResponse(answer));
          }
          /*
          else if (questions[n].getType() == 'CHECKBOX'){
            formResponse = formResponse.withItemResponse(questions[n].asCheckboxItem().createResponse(answer));
          }
          else if (questions[n].getType() == 'MULTIPLE_CHOICE'){
            formResponse = formResponse.withItemResponse(questions[n].asMultipleChoiceItem().createResponse(answer));
          }*/
        }
      }
    }
  formResponse.submit();
              
    /*Logger.log('testArray: ' + testArray);
    for(var t = 0; t < testArray.length; t++){
      Logger.log('testArray Entry: ' + t + ': ' + testArray[t]);
    }*/
            
}
