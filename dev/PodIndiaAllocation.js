/* 
 * OWNER: douglascox, uxi-automation
 * SOURCES: go/studyqueue
 * Description: takes in namedValues to determine if Pod ("India" or "Apac"). Short-circuit existing logic to augment pod if proper conditions other behaves as normal
 * @param Object (e.namedValues)
 * @return Boolean (string || null)
 */
function podAllocationIndiaApac(namedValues) {
  const STATIC = {
    Question: {
      ParticipantCategory: "Do your participants belong to any of the following categories?",
      Country: "Where in the world do you want the participants in your study to be located?"
    },
    Answer: {
      ParticipantCategory: {
        IndiaOffline: "India Offline Users (Fully Offline Users with Hindi, Bengali as study languages)",
        IndiaNIU: "India NIU Users (New Internet Users with Hindi, Bengali as study languages)",
        IndiaEIU: "India EIU Users (Experienced Internet Users with English or Hindi as study language)",
        // None: "None of the above (since question is mandatory) - {Route based on existing logic by UXR location}"
      }
    }
  }

  const getHasOwnProperty = (props) => (str) => props.hasOwnProperty(str) ? props[str][0] : null

  return hasOnlyIndiaCountrySelected(namedValues)

  function hasOnlyIndiaCountrySelected(namedValues){
    const hasOwnNamedValues = getHasOwnProperty(namedValues)
     const countryAnswer = hasOwnNamedValues(STATIC.Question.Country).trim().toLowerCase();
     const participantCategoryAnswer = hasOwnNamedValues(STATIC.Question.ParticipantCategory);

     if( countryAnswer !== "india"){
       return null
     }
     if( toPodAPAC(participantCategoryAnswer)){
       return "Pod APAC"
     }
     if ( toPodIndia(participantCategoryAnswer)){
       return "Pod India"
     }
     return null

     function toPodIndia(answer){
         const isIndiaPod = Object.values(STATIC.Answer.ParticipantCategory).some( str => answer.includes(str)) ? true : false
         return isIndiaPod
     }
     function toPodAPAC(answer){
       const hasEIU = [STATIC.Answer.ParticipantCategory.IndiaEIU].some( str => answer.includes(str)) 
       const hasNonNIU = [STATIC.Answer.ParticipantCategory.IndiaNIU, STATIC.Answer.ParticipantCategory.IndiaOffline].some( str => answer.includes(str)) ? false : true 
       return hasEIU && hasNonNIU
     }
  }

}
