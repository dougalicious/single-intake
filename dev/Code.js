function createFormSubmission(e) {
  //if (e.range.columnStart == e.range.columnEnd) return;
  var ss = SpreadsheetApp.getActiveSpreadsheet()
  var sheet = SpreadsheetApp.getActiveSheet(); //form response sheet
  var msheet = ss.getSheetByName('Question mapping'); //map sheet
  Logger.log(sheet.getName());
  try {
    var lock = LockService.getScriptLock();
    lock.waitLock(60000);  // wait 60 seconds before timing out
    if (!lock.hasLock()) {//Failed to get lock
      /*MailApp.sendEmail(Session.getEffectiveUser().getEmail(),
      'Code Failed', 'The code for survey submission failed');*/
      Logger.log('Could not obtain lock');
      return;
    }
    for (i = 1; i < 4; i++) {//Try up to 3 times
      try {
        if (sheet.getName() == 'Form Responses') {
          Logger.log('Form Responses');
          singleIntakeMapping(ss, sheet, msheet, e);
        }
        else if (sheet.getName() == 'FR to R-Ops') { //Triage to ROps
          Logger.log('FR to R-Ops');
          toRops(ss, sheet, msheet, e);
        }
        else if (sheet.getName() == 'R-Ops to FR') { //Triage to FR
          Logger.log('R-Ops to FR');
          toFR(ss, sheet, msheet, e);
        }
        /*else{
          break;
        }*/
        break;
      }
      catch (e) {
        if (i !== 3) { Utilities.sleep(i * 2000); }
        if (i >= 3) {
          /*MailApp.sendEmail(Session.getEffectiveUser().getEmail(), 
           'Code Failed', 'The code for Mapping failed - ' + e.message + "\n\n" +
            e.stack);*/
          Logger.log('error: ' + e.message + ' stack: ' + e.stack);
          
          throw(e)
        }
      };
    }

    //Code here

    lock.releaseLock();


  } catch (e) {
    lock.releaseLock();//Release the lock if there is an error
    /*MailApp.sendEmail(Session.getEffectiveUser().getEmail(), 
      'Code Failed', 'The code for survey submission routing failed - ' + e.message + "\n\n" +
       e.stack);*/
    Logger.log('error: ' + e.message + ' stack: ' + e.stack);
    throw(e)
  }
}


function singleIntakeMapping(ss, sheet, ms, e) {

  var mapValues = ms.getDataRange().getValues();
  var msLastRow = ms.getLastRow();
  var ks = ss.getSheetByName('Question 1 Key'); //question 1 key sheet
  var keyValues = ks.getDataRange().getValues();
  var ksLastRow = ks.getLastRow();
  var map = {};
  for (var v = 1; v < ksLastRow; v++) {
    map[keyValues[v][0]] = keyValues[v][1];
  }

  var formResponse = e.range.getValues()[0];
  var activeRow = e.range.getRow(); //row_number
  Logger.log('activeRow: ' + activeRow);
  e = fetchAndSetWhomWeSupportMetric(e);
 
  var values = e.namedValues; //headers and form submission answers
  /** includes orange column data  */
  var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
  var headers2 = sheet.getRange(2, 1, 1, sheet.getLastColumn()).getValues()[0];

  //---------------------------------------------------------------------
  //question 1 mapping
  var qMap = {};
  
  var supportTypes = values[map["rSupport"]][0];
  Logger.log('supportTypes: ' + supportTypes);
  var ootoAnswer = values[map["ootoSupport"]][0]; //yes/no
  Logger.log('ootoAnswer: ' + ootoAnswer);
  // getAgencyStudyId();
  /**** multivitiam 6.7 */
  const isSupportTypeROps = supportTypes === "rOps"
  const getAgency = (str) => Object.assign({}, {agency: 'infosys'})
  
  for (var v = 0; v < keyValues.length; v++) {
    // v=row, 1="Script Variable"
    if (keyValues[v][1] == supportTypes.trim() && keyValues[v][2] == ootoAnswer.trim()) {
      supportTypes = keyValues[v][0];
      Logger.log('supportTypes: ' + supportTypes);
      /*** INCLUDE FORM ID ANSWER LAB */
      /** adjustment made detemented on Pod (inforsys|answerlab) */
      
      let selectStudyQueue = ({namedValues}) =>  {
        const [pod] = namedValues?.Pod 
        return /al/gi.test(pod) ? keyValues[v][4] : keyValues[v][3]
        }
      // var formId = keyValues[v][3];
      var formInfoSys = keyValues[v][3];
      var formId = selectStudyQueue(e)
      
      Logger.log('formId: ' + formId);
    }
  }

  /** update to Pod India if applicable ONLY for Infosys*/
  let isIndiaApacPod = podAllocationIndiaApac(e.namedValues);
  const isGoStudyqueue = formInfoSys === formId
  const updatePodAnswer = (str) => e.namedValues.Pod = [str]
  (!!isIndiaApacPod && isGoStudyqueue) && updatePodAnswer(isIndiaApacPod) // : supported[3]
  
  
  qMap["branch"] = mapValues[0].indexOf(supportTypes); //getting column number from matching the question 1 answer to the mapping header
  Logger.log('qMap["branch"]: ' + qMap["branch"]);

  //---------------------------------------------------------------------   
  //Make the mapping array
  Logger.log('values[map["rSupport"]][0]: ' + values[map["rSupport"]][0]);
  var array = [];
  var mapColumn = mapValues[0].indexOf(qMap["branch"]);
  Logger.log('mapColumn: ' + mapColumn);

  for (var i = 0; i < headers.length; i++) {
    if (values[headers[i]] != '') {
      for (var j = 1; j < msLastRow; j++) {
        if (mapValues[j][0] == headers[i] && headers2[i] == supportTypes || mapValues[j][0] == headers[i] && headers2[i] == "rSupport") {
          var line = [];
          line.push(mapValues[j][0]); //Field Name column
          line.push(mapValues[j][qMap["branch"]]); //appropriate branch column
          line.push(i);
          //Logger.log(line);
          array.push(line);
        }
      }
    }
  }

  //get the question id's for each of the questions and map based on that

  Logger.log('array: ' + array);
    Logger.log('formId: ' + formId);
  //---------------------------------------------------------------------   
  //Map the answers to the correct form question and submit the form
  debugger
  var form = FormApp.openByUrl(formId);

  var questions = form.getItems();
  var formResponse = form.createResponse();
  for (var n = 0; n < questions.length; n++) {
    for (var k = 0; k < array.length; k++) {
      if (array[k][1] == questions[n].getTitle()) {
        var mappingQ = array[k][0];
        Logger.log('mappingQ: ' + mappingQ);
        //var column = header.indexOf(mappingQ)
        var column = array[k][2];
        Logger.log('column: ' + column);
        var answer = sheet.getRange(activeRow, column + 1, 1, 1).getValue(); //Add something to grab a blank for space holder
        Logger.log('answer: ' + answer);
        let questionType = questions[n].getType()
        if (questions[n].getType() == 'TEXT') {
          formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
        } else if (questions[n].getType() == "PARAGRAPH_TEXT") {
          formResponse = formResponse.withItemResponse(questions[n].asParagraphTextItem().createResponse(answer));
        } else if (questions[n].getType() == 'DATE') {
          formResponse = formResponse.withItemResponse(questions[n].asDateItem().createResponse(answer));
        } else if (questionType = "LIST"){
          formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
        }
      }
    }
  }
  debugger
  formResponse.submit();
  Logger.log('form submited');
   debugger
  sendEmailStudyReceipt(e)
  
}


function toRops(ss, sheet, mSheet, e) {
  var mapValues = mSheet.getDataRange().getValues();
  var msLastRow = mSheet.getLastRow();

  var formResponse = e.range.getValues()[0];
  var activeRow = e.range.getRow(); //row_number
  var values = e.namedValues; //headers and form submission answers
  var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];

  //---------------------------------------------------------------------   
  //Make the mapping array

  var array = [];
  var fieldNameCol = mapValues[0].indexOf('Field Name');
  var rOpsCol = mapValues[0].indexOf('rOps');
  for (var i = 0; i < headers.length; i++) {
    if (values[headers[i]] != '') {
      for (var j = 1; j < msLastRow; j++) {
        if (mapValues[j][0] == headers[i]) {
          var line = [];
          line.push(mapValues[j][fieldNameCol]);
          line.push(mapValues[j][rOpsCol]);
          //Logger.log(line);
          array.push(line);
        }
      }
    }
  }

  Logger.log('array: ' + array);
  //---------------------------------------------------------------------   
  //Map the answers to the correct form question and submit the form
  /** Multivitiam env.dev */
  var frFromUrl = "https://docs.google.com/forms/d/1DpWd_wVdbws2dfA-ocH6USTXuQhEOp-LrKLQhuHkGQk/edit"
  var form = FormApp.openByUrl(frFromUrl);
  // var form = FormApp.openByUrl('https://docs.google.com/forms/d/1z3v-6YFtnNAGwLFVX3XaqKvPSY6eS2-gER_s8oxJ9xA/edit');
  var questions = form.getItems();
  var formResponse = form.createResponse();
  var isMatchFormTitleWithSSHeaderReference = (fromForm, fromSS) => (typeof fromForm === 'string' ? fromForm.trim() === fromSS.trim() : false)

  for (var n = 0; n < questions.length; n++) {
    for (var k = 0; k < array.length; k++) {

      // 04.22.20 added fn to capture match with paragraph_text 
      // 4.22.20 added or to the below to account for paragraph text type
      if (array[k][1] == questions[n].getTitle() || isMatchFormTitleWithSSHeaderReference(questions[n].getTitle(), array[k][1])) {
        var mappingQ = array[k][0];
        //   Logger.log('mappingQ: ' + mappingQ);
        var column = headers.indexOf(mappingQ);
        //   Logger.log('column: ' + column);
        var answer = sheet.getRange(activeRow, column + 1, 1, 1).getValue();
        //    Logger.log('answer: ' + answer);
        if (questions[n].getType() == 'TEXT') {
          formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
        }
        else if (questions[n].getType() == "PARAGRAPH_TEXT") {
          formResponse = formResponse.withItemResponse(questions[n].asParagraphTextItem().createResponse(answer));
        }
        else if (questions[n].getType() == 'DATE') {
          formResponse = formResponse.withItemResponse(questions[n].asDateItem().createResponse(answer));
        }
      }
    }
  }
  try {
    let preFillUrl = formResponse.toPrefilledUrl()
    Logger.log('prefillurl')
    Logger.log(preFillUrl)
  } catch (e) {
    Logger.log(e);
  }
  debugger
  formResponse.submit();
}


function toFR(ss, sheet, mSheet, e) {
  var mapValues = mSheet.getDataRange().getValues();
  var msLastRow = mSheet.getLastRow();

  var formResponse = e.range.getValues()[0];
  var activeRow = e.range.getRow(); //row_number
  var values = e.namedValues; //headers and form submission answers
  var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];

  //---------------------------------------------------------------------   
  //Make the mapping array

  var array = [];
  var fieldNameCol = mapValues[0].indexOf('Field Name');
  var fResearchCol = mapValues[0].indexOf('fResearch');
  for (var i = 0; i < headers.length; i++) {
    if (values[headers[i]] != '') {
      for (var j = 1; j < msLastRow; j++) {
        if (mapValues[j][0] == headers[i]) {
          var line = [];
          line.push(mapValues[j][fieldNameCol]);
          line.push(mapValues[j][fResearchCol]);
          //Logger.log(line);
          array.push(line);
        }
      }
    }
  }
  Logger.log('array: ' + array);

  //---------------------------------------------------------------------   
  //Map the answers to the correct form question and submit the form
  /** Multivitiam env.dev dev go/ftrix form */
  var form = FormApp.openByUrl('https://docs.google.com/forms/d/1egQZSsspmAPdtvld1Rrgz_zZ2ZfWJ6mGbMWpeVoXgRE/edit')
  // var form = FormApp.openByUrl('https://docs.google.com/forms/d/1-ElqzbnR3vUoP90icGPB8GPmlZt8Nha2GzKetX5Dtvg/edit');
  var questions = form.getItems();
  var formResponse = form.createResponse();
  for (var n = 0; n < questions.length; n++) {
    for (var k = 0; k < array.length; k++) {
      let questionType = questions[n].getType()
      if (array[k][1] == questions[n].getTitle()) {
        var mappingQ = array[k][0];
        //            Logger.log('mappingQ: ' + mappingQ);
        var column = headers.indexOf(mappingQ);
        //            Logger.log('column: ' + column);
        var answer = sheet.getRange(activeRow, column + 1, 1, 1).getValue();
        //            Logger.log('answer: ' + answer);
        if (questions[n].getType() == 'TEXT') {
          formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
        }
        else if (questions[n].getType() == "PARAGRAPH_TEXT") {
          formResponse = formResponse.withItemResponse(questions[n].asParagraphTextItem().createResponse(answer));
        }
        else if (questions[n].getType() == 'DATE') {
          formResponse = formResponse.withItemResponse(questions[n].asDateItem().createResponse(answer));
        } else if (questionType = "LIST"){
          formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
        }
      }
    }
  }
  try {
    let preFillUrl = formResponse.toPrefilledUrl()
    Logger.log('prefillurl')
    Logger.log(preFillUrl)
  } catch (e) {
    Logger.log('unable to capture prefill')
  }
  formResponse.submit();
}



//email sent if a request comes in that is 'recruitment only' and they are on WWS list
function recruitmentOnlyFlag() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName('Form Responses');
  var formResponse = e.range.getValues()[0];
  var activeRow = e.range.getRow(); //row_number
  var values = e.namedValues; //headers and form submission answers
  var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];

  var colA = values[0].indexOf('Timestamp') + 1;
  var colB = values[0].indexOf('Email Address') + 1;
  var colG = values[0].indexOf('Which services do you need?') + 1;
  var colCC = values[0].indexOf('Business Unit') + 1;

  //Whom we support sheet and information
  var wwsSheet = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A/edit#gid=721091946');
  var wwss = wwsSheet.getSheetByName('All ldaps');
  var wwssData = wwss.getDataRange();
  var wwssValues = wwssData.getValues();
  var wwssLastRow = wwss.getLastRow();
  Logger.log('wwssLastRow: ' + wwssLastRow);

  var wwsColA = wwssValues[0].indexOf('Ldap') + 1;
  var wwsColE = wwssValues[0].indexOf('Business Unit') + 1;
  var wwsColI = wwssValues[0].indexOf('Pod') + 1;


  var email = sheet.getRange(activeRow, colB, 1, 1).getValue();
  var businessUnit = sheet.getRange(activeRow, colCC, 1, 1).getValue();
  var pod = sheet.getRange(activeRow, colCC + 1, 1, 1).getValue();
  var ldap = email.split('@');
  var services = sheet.getRange(activeRow, colG, 1, 1).getValue();

  var array = keepUnique(values, wwss);

  if (businessUnit == '' || pod == '') {
    for (var j = 0; j <= array.length; j++) {
      try {
        if (ldap[0] == array[j][0]) {
          var wwssBusinessUnit = wwss.getRange(array[j][1], wwsColE, 1, 1).getValue();
          Logger.log('businessUnit: ' + wwssBusinessUnit);
          sheet.getRange(i, colCC, 1, 1).setValue(wwssBusinessUnit);

          var wwssPod = wwss.getRange(array[j][1], wwsColI, 1, 1).getValue();
          Logger.log('pod: ' + wwssPod);
          sheet.getRange(i, colCC + 1, 1, 1).setValue(wwssPod);

          try {
            if (services = 'Participant recruitment and incentives') {
              //email grace, bertie, jake
              //emailToRecipient(subject, message, recipient, ccAddress)
              //flag column that it has been sent to Grace
            }
          }
          catch (err) {
            Logger.log(catchToString(err));
          }
        }
      }
      catch (err) {
        Logger.log(catchToString(err));

      }
    }
  }

}


function getBUandPod() { //single use that checks all rows
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName('Form Responses');
  var values = sheet.getDataRange().getValues();
  var lastRow = sheet.getLastRow();

  //Form Responses 4 columns
  var colA = values[0].indexOf('Timestamp') + 1;
  var colB = values[0].indexOf('Email Address') + 1;
  var colCC = values[0].indexOf('Business Unit') + 1;

  //Whom we support list
  var wwsSheet = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A/edit#gid=721091946');
  var wwss = wwsSheet.getSheetByName('All ldaps');
  var wwssData = wwss.getDataRange();
  var wwssValues = wwssData.getValues();
  var wwssLastRow = wwss.getLastRow();
  Logger.log('wwssLastRow: ' + wwssLastRow);

  var wwsColA = wwssValues[0].indexOf('Ldap') + 1;
  var wwsColE = wwssValues[0].indexOf('Business Unit') + 1;
  var wwsColI = wwssValues[0].indexOf('Pod') + 1;

  var array = keepUnique(values, wwss);

  //check to see if it is recruitment only
  //check to see if they are in WWS
  //email if they are

  for (var i = 2; i <= lastRow; i++) {
    var email = sheet.getRange(i, colB, 1, 1).getValue();
    var businessUnit = sheet.getRange(i, colCC, 1, 1).getValue();
    var pod = sheet.getRange(i, colCC + 1, 1, 1).getValue();
    var ldap = email.split('@');

    if (businessUnit == '' || pod == '') {
      for (var j = 0; j <= array.length; j++) {
        try {
          if (ldap[0] == array[j][0]) {
            var wwssBusinessUnit = wwss.getRange(array[j][1], wwsColE, 1, 1).getValue();
            Logger.log('businessUnit: ' + wwssBusinessUnit);
            sheet.getRange(i, colCC, 1, 1).setValue(wwssBusinessUnit);

            var wwssPod = wwss.getRange(array[j][1], wwsColI, 1, 1).getValue();
            Logger.log('pod: ' + wwssPod);
            sheet.getRange(i, colCC + 1, 1, 1).setValue(wwssPod);
          }
        }
        catch (err) {
          Logger.log(catchToString(err));

        }
      }
    }
  }
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------------------Utility Functions------------------------------------------//
/////////////////////////////////////////////////////////////////////////////////////////////////////////


function keepUnique(values, wwss) {
  var col = 32; // email address column index

  var colA = values[0].indexOf('Timestamp') + 1;
  var colB = values[0].indexOf('Email Address') + 1;
  var colCC = values[0].indexOf('Cost Center') + 1;

  var newdata = new Array();
  for (nn in values) {
    var duplicate = false;
    for (j in newdata) {
      if ((values[nn][col].split('@'))[0] == newdata[j][0]) {
        duplicate = true;
      }
    }
    if (!duplicate) {
      newdata.push([(values[nn][col].split('@'))[0]]);
    }
  }
  /* Logger.log(newdata); //sorting function, not needed here
 newdata.sort(function(x,y){
   var xp = Number(x[0]);// ensure you get numbers
   var yp = Number(y[0]);
   return xp == yp ? 0 : xp < yp ? -1 : 1;// sort on numeric ascending
 });*/
  Logger.log(newdata);
  var array = rowOfCode2(newdata, wwss);
  Logger.log('array [1][1]: ' + array[1]);
  return array;
}

function rowOfCode2(ldapArray, wwss) {
  var wwssValues = wwss.getDataRange().getValues();
  var wwssLastRow = wwss.getLastRow();

  var array = [];
  for (var i = 0; i < wwssLastRow; i++) {
    for (var j = 1; j <= ldapArray.length; j++) {
      if (wwssValues[i][0] == ldapArray[j]) {
        //Logger.log((i+1))
        //Logger.log(ldapArray[j])
        var line = [];
        line.push(ldapArray[j]);
        line.push(i + 1);
        Logger.log(line);
        array.push(line);
        break;
      }
      //else{var row = -1}
    }
  }
  Logger.log('array 1: ' + array);
  return array;
}


function emailToRecipient(subject, message, recipient, ccAddress) { //Generic email sending function
  var mailFromIndex = GmailApp.getAliases().indexOf('uxi-alerts@google.com'); //OOTO alias
  var mailFrom = GmailApp.getAliases()[mailFromIndex];
  GmailApp.sendEmail(recipient, subject, message, { from: mailFrom, cc: ccAddress, replyTo: recipient, htmlBody: message });
  // GmailApp.sendEmail(recipient, subject, message, { from: mailFrom, cc: ccAddress, replyTo: recipient, htmlBody: message });
  Logger.log(message);
}

function catchToString(err) { //Granular error message
  var errInfo = "Done Catched Something:  (╯°□°)╯︵ ┻━┻\n";
  for (var prop in err) {
    errInfo += "  property: " + prop + "\n    value: [" + err[prop] + "]\n";
  }
  errInfo += "  toString(): " + " value: [" + err.toString() + "]";
  return errInfo;
}