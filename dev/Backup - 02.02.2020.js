function createFormSubmission_bk_02022020(){
  //if (e.range.columnStart == e.range.columnEnd) return;

    var ss = SpreadsheetApp.getActiveSpreadsheet()
    var sheet = SpreadsheetApp.getActiveSheet(); //form response sheet
    
    var ms = ss.getSheetByName('Question mapping'); //map sheet
    var mapValues = ms.getDataRange().getValues();
    var msLastRow = ms.getLastRow();
    
    var ks = ss.getSheetByName('Question 1 Key'); //question 1 key sheet
    var keyValues = ks.getDataRange().getValues();
    var ksLastRow = ks.getLastRow();
    var map = {};
    for (var v = 1; v<ksLastRow; v++){
      map[keyValues[v][0]] = keyValues[v][1];
    }

    var formResponse = e.range.getValues()[0];
    var activeRow = e.range.getRow(); //row_number
      Logger.log('activeRow: ' + activeRow);
    var values = e.namedValues; //headers and form submission answers
    var headers = sheet.getRange(1,1,1,sheet.getLastColumn()).getValues()[0];
    var headers2 = sheet.getRange(2,1,1,sheet.getLastColumn()).getValues()[0];
    
//---------------------------------------------------------------------
//question 1 mapping
   var qMap = {};
   var supportTypes = values[map["rSupport"]][0];
     Logger.log('supportTypes: ' + supportTypes);
   var ootoAnswer = values[map["ootoSupport"]][0]; //yes/no
     Logger.log('ootoAnswer: ' + ootoAnswer);
    for(var v=0; v<keyValues.length; v++){
      if(keyValues[v][1] == supportTypes.trim() && keyValues[v][2] == ootoAnswer.trim()){
        supportTypes = keyValues[v][0];
          Logger.log('supportTypes: ' + supportTypes);
        var formId = keyValues[v][3];
          Logger.log('formId: ' + formId);
      }
    }

    qMap["branch"] = mapValues[0].indexOf(supportTypes); //getting column number from matching the question 1 answer to the mapping header
      Logger.log('qMap["branch"]: ' + qMap["branch"]);

//---------------------------------------------------------------------   
//Make the mapping array
      Logger.log('values[map["rSupport"]][0]: ' + values[map["rSupport"]][0]);
    var array = [];
    var mapColumn = mapValues[0].indexOf(qMap["branch"]);
      Logger.log('mapColumn: ' + mapColumn);
    
    for(var i = 0; i < headers.length; i++){
      if(values[headers[i]] != ''){
        for(var j = 1; j < msLastRow; j++){
          if(mapValues[j][0] == headers[i] && headers2[i] == supportTypes || mapValues[j][0] == headers[i] && headers2[i] == "rSupport"){
            var line = [];
            line.push(mapValues[j][0]); //Field Name column
            line.push(mapValues[j][qMap["branch"]]); //appropriate branch column
            line.push(i);
            //Logger.log(line);
            array.push(line);
          }
        }
      }
    }
    
    //get the question id's for each of the questions and map based on that
    
      Logger.log('array: ' + array); 
//---------------------------------------------------------------------   
//Map the answers to the correct form question and submit the form
    var form = FormApp.openByUrl(formId);
    var questions = form.getItems();
    var formResponse = form.createResponse();
    for(var n = 0; n < questions.length; n++){
      for(var k = 0; k < array.length; k++){
        if(array[k][1] == questions[n].getTitle()){
          var mappingQ = array[k][0];
            Logger.log('mappingQ: ' + mappingQ);
          //var column = header.indexOf(mappingQ)
          var column = array[k][2];
            Logger.log('column: ' + column);
          var answer = sheet.getRange(activeRow,column+1,1,1).getValue(); //Add something to grab a blank for space holder
            Logger.log('answer: ' + answer);
          if (questions[n].getType() == 'TEXT'){
            formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
          }
          else if (questions[n].getType() == 'DATE'){
            formResponse = formResponse.withItemResponse(questions[n].asDateItem().createResponse(answer));
          }
        }
      }
    }
    
  formResponse.submit(); 

}
