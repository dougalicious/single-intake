function multivitiamtest() {
  // let response = Browser.inputBox(`Select a Pod\n NOTE: having "al" or "AL" within the pod name defined it to be for answerlab`)
  // this.podName = response
  let e = artificalEventGenerator();
  let c = createFormSubmission(e)
  debugger
}
function getRangeList() {
  const rangeList = SpreadsheetApp.getActiveRangeList().getRanges()
  const arr = rangeList.flatMap(range => {
    const row = range.getRow()
    const lastRow = range.getLastRow();
    let rowIndices = []
    for (let i = row; i <= lastRow; i++) {

      rowIndices.push(i)
    }
    return row === lastRow ? row : rowIndices
  })
  debugger
  Logger.log(JSON.stringify(arr))
  arr.forEach(testingFRconnectionFromSingleIntake)
}



function testingFRconnectionFromSingleIntake(rowIdx) {
  var ss = SpreadsheetApp.getActiveSpreadsheet()
  var sheet = ss.getSheetByName("Form Responses")
  var msheet = ss.getSheetByName('Question mapping');
  // var range = sheet.getRange(2185, 1, 6, sheet.getLastColumn());
  // var rowIdx = ss.getActiveRange().getRow()
  Logger.log(`rowIdx: ${rowIdx}`)
  var range = sheet.getRange(rowIdx, 1, 1, sheet.getLastColumn())
  var values = range.getValues()
  values = values.slice(0, 1) //, ...values.slice(2)];

  var namedValues = getHeaderObj(sheet, values)
  //var namedValues = 
  var e = { range: range, namedValues }
  debugger
  var nv = e.namedValues

  manual_singleIntakeMapping(ss, sheet, msheet, e);

  function manual_singleIntakeMapping(ss, sheet, ms, e) {

    var mapValues = ms.getDataRange().getValues();
    var msLastRow = ms.getLastRow();

    var ks = ss.getSheetByName('Question 1 Key'); //question 1 key sheet
    var keyValues = ks.getDataRange().getValues();
    var ksLastRow = ks.getLastRow();
    var map = {};
    for (var v = 1; v < ksLastRow; v++) {
      map[keyValues[v][0]] = keyValues[v][1];
    }

    var formResponse = e.range.getValues()[0];
    var activeRow = e.range.getRow(); //row_number
    // Logger.log('activeRow: ' + activeRow);
    var values = e.namedValues; //headers and form submission answers
    var headers = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
    var headers2 = sheet.getRange(2, 1, 1, sheet.getLastColumn()).getValues()[0];

    //---------------------------------------------------------------------
    //question 1 mapping
    var qMap = {};
    // what type of research
    var supportTypes = values[map["rSupport"]][0];
    // Logger.log('supportTypes: ' + supportTypes);
    var ootoAnswer = values[map["ootoSupport"]][0]; //yes/no
    debugger
    // Logger.log('ootoAnswer: ' + ootoAnswer);
    for (var v = 0; v < keyValues.length; v++) {
      if (keyValues[v][1] == supportTypes.trim() && keyValues[v][2] == ootoAnswer.trim()) {
        supportTypes = keyValues[v][0];
        // Logger.log('supportTypes: ' + supportTypes);
        var formId = keyValues[v][3];
        //   formId="https://docs.google.com/forms/d/1-ElqzbnR3vUoP90icGPB8GPmlZt8Nha2GzKetX5Dtvg/edit"
        debugger
        // Logger.log('formId: ' + formId);
      }
    }

    qMap["branch"] = mapValues[0].indexOf(supportTypes); //getting column number from matching the question 1 answer to the mapping header
    // Logger.log('qMap["branch"]: ' + qMap["branch"]);
    //---------------------------------------------------------------------   
    //Make the mapping array
    // Logger.log('values[map["rSupport"]][0]: ' + values[map["rSupport"]][0]);
    var array = [];
    var mapColumn = mapValues[0].indexOf(qMap["branch"]);
    // Logger.log('mapColumn: ' + mapColumn);

    for (var i = 0; i < headers.length; i++) {
      if (values[headers[i]] != '') {
        for (var j = 1; j < msLastRow; j++) {
          if (mapValues[j][0] == headers[i] && headers2[i] == supportTypes || mapValues[j][0] == headers[i] && headers2[i] == "rSupport") {
            var line = [];
            line.push(mapValues[j][0]); //Field Name column
            line.push(mapValues[j][qMap["branch"]]); //appropriate branch column
            line.push(i);
            //Logger.log(line);
            array.push(line);
          }
        }
      }
    }

    //get the question id's for each of the questions and map based on that

    // Logger.log('array: ' + array);
    //---------------------------------------------------------------------   
    //Map the answers to the correct form question and submit the form
    // Test current product (UI, ID, visual design etc)
    var form = FormApp.openByUrl(formId);
    var questions = form.getItems();
    //    var qwithTittle = questions.map( q => q.getTitle() );
    //    var qwithType = questions.map( q => q.getType().toString() );
    var isMatchFormTitleWithSSHeaderReference = (fromForm, fromSS) => (typeof fromForm === 'string' ? fromForm.trim() === fromSS.trim() : false)

    // debugger
    var formResponse = form.createResponse();
    for (var n = 0; n < questions.length; n++) {  ///loop through question
      for (var k = 0; k < array.length; k++) {    /// loop through active row
        var abcquestionTitle = questions[n].getTitle() // from form
        var fromArray = array[k][1] // spreadsheet
        var isequalllz = array[k][1] == questions[n].getTitle()
        var twosame = [questions[n].getTitle().toString(), isequalllz = array[k][1]].map((txt) => { return typeof txt; })
        var boo = isMatchFormTitleWithSSHeaderReference(questions[n].getTitle(), array[k][1])

        if (questions[n].getTitle() == "What decisions or changes do you expect to make as a result of this study? ") {
          isMatchFormTitleWithSSHeaderReference(question[n].getTitle(), array[k][1])
        }
        // isMatchFormTitleWithSSHeaderReference(question[n].getTitle(), array[k][1])
        if (array[k][1] == questions[n].getTitle() || isMatchFormTitleWithSSHeaderReference(questions[n].getTitle(), array[k][1])) {

          var mappingQ = array[k][0];
          // Logger.log('mappingQ: ' + mappingQ);
          //var column = header.indexOf(mappingQ)
          var column = array[k][2];
          // Logger.log('column: ' + column);
          var answer = sheet.getRange(activeRow, column + 1, 1, 1).getValue(); //Add something to grab a blank for space holder
          // Logger.log('answer: ' + answer);
          let gtype = questions[n].getType()
          //debugger

          if (questions[n].getType() == 'TEXT') {
            let ans = answer
            //debugger
            formResponse = formResponse.withItemResponse(questions[n].asTextItem().createResponse(answer));
          }
          if (questions[n].getType() == "PARAGRAPH_TEXT") {
            formResponse = formResponse.withItemResponse(questions[n].asParagraphTextItem().createResponse(answer));
          }
          else if (questions[n].getType() == 'DATE') {
            formResponse = formResponse.withItemResponse(questions[n].asDateItem().createResponse(answer));
          }
        }
      }
    }
    let preFillUrl = formResponse.toPrefilledUrl()
    Logger.log('prefillurl')
    Logger.log(preFillUrl)
    debugger
    formResponse.submit();

  }

}
function getHeaderObj(sheet, values) {
  var dataRange = sheet.getDataRange()
  var header = dataRange.getValues().slice(0, 1)[0]
  return header.reduce((acc, next, idx) => {
    acc[next] = [values[0][idx]]
    return acc
  }, {});
}



function populateBlankStudyoutcomesonGOFRTRIX() {
  var cpgofrtrix = getGoFrTrix().body
  cpgofrtrix.pop()
  cpgofrtrix = cpgofrtrix.reverse().filter(rowAsObj => rowAsObj["Study Outcomes"] === "");

  var uxiSupportrqform = getCurrentss().body
  uxiSupportrqform.pop()
  uxiSupportrqform = uxiSupportrqform.reverse().filter(rowAsObj => rowAsObj["Research project name"] !== "")
  let dropCount = 0
  cpgofrtrix.forEach((rowAsObj, idx) => {
    let ldap = rowAsObj["Requester"]
    let projectName = rowAsObj["Project Name"]
    let found = uxiSupportrqform.find((supForm) => supForm["Email Address"] == ldap && supForm["Research project name"] == projectName);
    let foundIdx = uxiSupportrqform.findIndex((supForm) => supForm["Email Address"] == ldap && supForm["Research project name"] == projectName)

    if (!!found) {
      debugger
      let drop = uxiSupportrqform.splice(foundIdx, 1)
      dropCount += 1
      Logger.log(`update ${rowAsObj["Project ID"]}: ${found["Research project name"]} `)
      let studyOutComes = drop[0]["What decisions or changes do you expect to make as a result of this study?"]
      Logger.log(JSON.stringify(drop[0]["Research project name"]))
      let studyOutComesRange = rowAsObj.getRangeByColHeader("Study Outcomes");
      let currValue = studyOutComesRange.getValue();
      var a1 = studyOutComesRange.getA1Notation();
      debugger
      studyOutComesRange.setValue(studyOutComes)
    }
    debugger

  })


  //var blankStudyOutComes = getGoFrTrix.body.filter((rowAsObj) => rowAsObj["Study Outcomes"] === "")
  function getGoFrTrix() {
    var ss = SpreadsheetApp.openById("1lbWGTOB1WD9Ry2RxYby9nBNT18x-1oA7AOb344XTjDQ")
    //   var ss = SpreadsheetApp.openById("1fys-pkvO1__G6nP_a0pROIgxrl6mb6ZNWxo-pkSkQuQ")
    var sheet = ss.getSheetByName("Form Responses 2");
    return getDataBySheet(sheet, 0, 3);
  }

  function getCurrentss() {
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = ss.getSheetByName("Form Responses")
    return getDataBySheet(sheet, 0, 3, true);
  }
  /* returns properties of sheet
  * @param Object({Sheet})
  * @return { dataRange, body, headerObj, header, getValueByHeader }
  */
  function getDataBySheet(sheet, headerIdx = 0, bodyRowStart = 1) {
    var dataRange = sheet.getDataRange();
    var values = dataRange.getValues()
    var headers = values.slice(headerIdx, headerIdx + 1)[0];
    var body = values.slice(bodyRowStart)
    var headerObj = headers.reduce(makeHeaderObj, {})
    var a = body.map(getValueByHeader)
    return {
      dataRange,
      body: body.map(getValueByHeader),
      headerObj,
      headers,
      getValueByHeader,
    }
    function makeHeaderObj(acc, next, idx) {
      if (next === "" || next === "-") {
        return acc;
      }
      if (acc.hasOwnProperty(next)) {
        debugger
        throw ("Duplicate headers found: " + next)
      }
      acc[next] = idx
      return acc;
    }
    // transform Array(row) => Object()
    function getValueByHeader(row, rowIdx) {
      var rowAsObject = Object.create(headerObj)
      for (var header in headerObj) {
        var idx = headerObj[header]
        rowAsObject[header] = row[idx]
      }
      Object.defineProperties(rowAsObject, {
        getRangeByColHeader: {
          value: ((header) => sheet.getRange(rowIdx + 1 + bodyRowStart, headerObj[header] + 1)), // return range of column 4
          writable: false,
          enumerable: false,
        },
      })

      return rowAsObject
    }
  }
  /* returns properties of sheet
* @param Object({Sheet})
* @return { dataRange, body, headerObj, header, getValueByHeader }
*/
  function getDataBySheet(sheet, headerIdx = 0, bodyRowStart = 1, filterCB) {
    var dataRange = sheet.getDataRange();
    var values = dataRange.getValues()
    var headers = values.slice(headerIdx, headerIdx + 1)[0].slice(0, 20);
    var body = values.slice(bodyRowStart)
    var headerObj = headers.reduce(makeHeaderObj, {})
    var a = body.map(getValueByHeader)
    return {
      dataRange,
      body: body.map(getValueByHeader),
      headerObj,
      headers,
      getValueByHeader,
    }
    function makeHeaderObj(acc, next, idx) {
      if (next === "" || next === "-") {
        return acc;
      }
      if (acc.hasOwnProperty(next)) {
        debugger
        throw ("Duplicate headers found")
      }
      acc[next] = idx
      return acc;
    }
    // transform Array(row) => Object()
    function getValueByHeader(row, rowIdx) {
      var rowAsObject = Object.create(headerObj)
      for (var header in headerObj) {
        var idx = headerObj[header]
        rowAsObject[header] = row[idx]
      }
      Object.defineProperties(rowAsObject, {
        getRangeByColHeader: {
          value: ((header) => sheet.getRange(rowIdx + 1 + bodyRowStart, headerObj[header] + 1)), // return range of column 4
          writable: false,
          enumerable: false,
        },
      })

      return rowAsObject
    }
  }

}